import React,  {Component} from 'react';
import { BrowserRouter, Switch, Route} from "react-router-dom";
import  Home  from '../Container/Home/Home';
import  Login  from '../Container/Login/Login';
import  Profile  from '../Container/Profile/Profile';

class Routes extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/login' component={Login}/>
            <Route exact path='/profile' component={Profile}/>
            <Route exact path='/:id' component={Home}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default Routes;