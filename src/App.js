import React from 'react';
import  Routes  from './Routes/Routes';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <div className="App">
      <Routes></Routes>
    </div>
  );
}

export default App;
