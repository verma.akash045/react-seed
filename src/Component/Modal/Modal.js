import React, { Fragment, useState } from "react";
import { Modal, Button } from 'react-bootstrap';

import './Modal.css';

let ModalUI = (props) => {

    let [showModal, setShowModal] = useState(false)

    let handleClose = () => {

    }

    let handleSubmit = () => {
        setShowModal(false)
    }

    return (
        <div >
            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header>
                    <Modal.Title>Title</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <h1>Body</h1>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={() => handleClose} variant="secondary">Close</Button>
                    <Button onClick={() => handleSubmit} variant="primary">Save changes</Button>
                </Modal.Footer>
            </Modal>
        </div>

    );
}

export default ModalUI

