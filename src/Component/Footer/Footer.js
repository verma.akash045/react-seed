import React from 'react';
import {Navbar} from 'react-bootstrap';

const NavBar = (props) => {

    return (
        <div>
            <Navbar className="copyright" fixed="bottom" bg="primary" expand="lg" variant="dark">
                <span>Copyright &copy; React-Seed 2020</span>
            </Navbar>
        </div>
    );
}

export default NavBar;