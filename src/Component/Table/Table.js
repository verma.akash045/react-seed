import React from 'react';
import './Table.css'


let  Table = () =>  {
        return (
            <div className="container-fluid">
                <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                    <thead>
                        <tr>
                            <th>Heading1</th>
                            <th>Heading2</th>
                            <th>Headin3</th>
                            <th>Heading4</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Data1</td>
                            <td>Data2</td>
                            <td>data3</td>
                            <td>Data4</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
}
export default Table;