import React, { Component } from 'react';
import Paginate from "react-js-pagination";
import './Pagination.css';

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.state = {
      activePage:  1
    };
  }
 
  handlePageChange(pageNumber) {
    this.props.sendPageNumber(pageNumber);
    this.setState({activePage: pageNumber});
    }
 
  render() {

    return (
      <div className = 'body'>
        <Paginate
            activePage={this.state.activePage}
            itemsCountPerPage={this.props.entry}
            totalItemsCount={this.props.totalItemsCount}
            pageRangeDisplayed={3}
            onChange={this.handlePageChange}
            itemClass="page-item"
            linkClass="page-link"
            prevPageText='previous'
            nextPageText='next'
        />
      </div>
    );
  }
}
 

export default Pagination;