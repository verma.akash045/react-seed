import React, { Component } from 'react';
import '../../App.css';
import './Home.css';
import Navbar from '../../Component/NavBar/NavBar'
import Footer from '../../Component/Footer/Footer'

class Home extends Component {
  render() {
    return (
     <div>
       <Navbar></Navbar>
       <h1>Home</h1>
       <Footer></Footer>
     </div>
    );
  }
}
export default Home;