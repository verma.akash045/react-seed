import React from 'react';
import user from '../../Constant/Users'

class Login extends React.Component {
  
  constructor(props) {
    super(props)
    this.state = {
      user: {
        username: null,
        password: null
      }
    }
  }
  
  handleSubmit = () => {
    localStorage.clear()
    if(this.state.user.username == user[0].username && this.state.user.password == user[0].password){
      localStorage.setItem('username',this.state.user.username);
      localStorage.setItem('password',this.state.user.password);
      localStorage.setItem('isLoggedIn', true);
      this.props.props.history.push('/profile')
    }
    console.log("local storage -->", localStorage)
  }
  
  // signOut() {
  //   this.setState({user: null})
  // }
  
   handleChange = e => {
    const { name, value } = e.target
    this.setState({
      user: {
        ...this.state.user,
        [name]: value
      }
    }) 
}
  
  render() {
    return (
      <div>
          <label htmlFor="email">Username</label>
          <input name="username" onChange={this.handleChange}  type="text" placeholder="username" />

          <label htmlFor="email">Password</label>
          <input 
            name="password"
            onChange={this.handleChange}
            type="password"
            placeholder="Enter your password"
          />
          <button type="submit"  onClick={this.handleSubmit} >
            Login
          </button>
      </div>
    ) 
  }

}


export default Login;